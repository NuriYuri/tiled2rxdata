class TMXConverter
  # Show help to user
  def show_help
    puts 'Help'.center(80, '=')
    puts '> add tileset name png_filename : Add a new tileset with the specified name'
    puts '> add map tmx_filename tileset_name rmxp_map_id : Add a new map'
    puts '> reset tileset name : Reset the tileset (its tile may change position if reprocessed)'
    puts '> del tileset name : Delete tileset from project'
    puts '> del map tmx_filename : Delete the map (won\'t delete file)'
    puts '> convert tmx_filename : Trigger the map conversion'
    puts '> convert * : Trigger the map conversion of all maps in the project'
    puts '> build tileset : Trigger the tileset creation (png + rxdata)'
    puts '> list map : Show the list of map in the project'
    puts '> list tileset : Show the list of tilesets in the project'
    puts '> exit : Exit and save the data'
  end
end
